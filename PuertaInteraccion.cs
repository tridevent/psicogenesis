﻿using UnityEngine;
using System.Collections;

public class PuertaInteraccion : MonoBehaviour {
	private Animator Animaciones; // detecta la animaciones de la puerta
	private bool Esperar=false; //estado si se puede o no interactuar con la puerta
	private bool Estado=false;//Estado si la puerta esta abierta o cerrada
	private float Tiempo;// variable temporal para la comprobacion de la espera
	private GameObject ObjetoAnimado; // detecta el objeto colisionado en este caso la puerta
	private Transform HijoAnimacion; // detecta al hijo del objeto colisionado que es quien tiene la animacion
	public AudioClip Abrir_audio;
	public AudioClip Cerrar_audio;
	public AudioClip Intentar_audio;
	private Transform Puerta;
	private float Tiempo_audio;
	public GameObject[] ObjetoPuerta; // obj de las puertas
	private bool[] EsperarVec; 
	private bool[] EstadoVec; // el estado de las puertas
	private GameObject[] ObjetoPuertaAux; // vector que sirve para reacomodar el vector para que no queden huecos
	private bool[] EsperarVecAux; 
	private bool[] EstadoVecAux;
	private bool Chekeado=false;
	private int posicion=0; // la posicion sirve para mover los objetos dentro del vector

	void Update () {
		if(Esperar==true && Tiempo<Time.time)
		{
			Esperar=false;
			if(Chekeado==false)
			{
				EstadoVec[ObjetoPuerta.Length-1]= Estado;
				EsperarVec[ObjetoPuerta.Length-1]=Esperar;
			}
			else
			{
				EstadoVec[posicion]= Estado;
				EsperarVec[posicion]=Esperar;
			}
		}
	}
	void OnTriggerStay (Collider Col){
		if (Input.GetButtonUp ("interactuar")) 
		{
			if (Col.gameObject.tag == "Puerta") 
			{
				ObjetoAnimado= Col.GetComponent<Collider>().gameObject;
				//**************************************************************************
				Chekeado=false;
				if(ObjetoPuerta.Length>=1)
				{
					for(int i =0;i<=ObjetoPuerta.Length-1;i++)
					{
						if(ObjetoPuerta[i].GetInstanceID()== ObjetoAnimado.GetInstanceID())
						{
							Estado=EstadoVec[i];
							Esperar=EsperarVec[i];
							Chekeado=true;
							posicion=i;
							Puerta= ObjetoPuerta[i].gameObject.transform.GetChild(1);// abierta/cerrada
						}
					}
				}
				if(Chekeado==false)
				{
					if(ObjetoPuerta.Length>=1)
					{
						ObjetoPuertaAux= new GameObject[ObjetoPuerta.Length];
						EstadoVecAux= new bool[ObjetoPuerta.Length];
						EsperarVecAux= new bool[ObjetoPuerta.Length];
						for(int i=0;i<=ObjetoPuerta.Length-1;i++)
						{
							ObjetoPuertaAux[i]=ObjetoPuerta[i];
							EstadoVecAux[i]= EstadoVec[i];
							EsperarVecAux[i]= EsperarVec[i];
						}
					}
					ObjetoPuerta= new GameObject[ObjetoPuerta.Length+1];
					EstadoVec= new bool[ObjetoPuerta.Length];
					EsperarVec= new bool[ObjetoPuerta.Length];
					if(ObjetoPuerta.Length>=2)
					{
						for(int i=0;i<=ObjetoPuerta.Length-2;i++)
						{
							ObjetoPuerta[i]=ObjetoPuertaAux[i];
							EstadoVec[i]= EstadoVecAux[i];
							EsperarVec[i]= EsperarVecAux[i];
						}
					}
					ObjetoPuerta[ObjetoPuerta.Length-1]= ObjetoAnimado;
					Estado=false;
					Esperar=false;
					Puerta= ObjetoPuerta[ObjetoPuerta.Length-1].gameObject.transform.GetChild(1);// abierta/cerrada
				}
				//****************************************************************************
				if(Puerta.gameObject.activeSelf==false)
				{
					HijoAnimacion= ObjetoAnimado.gameObject.transform.GetChild(0);
					Animaciones=HijoAnimacion.GetComponent<Animator>();
					AnimacioPuerta();
				}
				else
				{
					if(Tiempo_audio+1<Time.time)
					{
						AudioSource.PlayClipAtPoint(Intentar_audio,transform.position);
						Tiempo_audio= Time.time;
					}
				}
			}
			if (Col.gameObject.tag == "TarjeteroPuerta") 
			{
				if(PuertaDigital.Permitido==true)
				{
					ObjetoAnimado= Col.GetComponent<Collider>().gameObject;
					//**************************************************************************
					Chekeado=false;
					if(ObjetoPuerta.Length>=1)
					{
						for(int i =0;i<=ObjetoPuerta.Length-1;i++)
						{
							if(ObjetoPuerta[i].GetInstanceID()== ObjetoAnimado.GetInstanceID())
							{
								Estado=EstadoVec[i];
								Esperar=EsperarVec[i];
								Chekeado=true;
								posicion=i;
								Puerta= ObjetoPuerta[i].gameObject.transform.GetChild(1);// abierta/cerrada
							}
						}
					}
					if(Chekeado==false)
					{
						if(ObjetoPuerta.Length>=1)
						{
							ObjetoPuertaAux= new GameObject[ObjetoPuerta.Length];
							EstadoVecAux= new bool[ObjetoPuerta.Length];
							EsperarVecAux= new bool[ObjetoPuerta.Length];
							for(int i=0;i<=ObjetoPuerta.Length-1;i++)
							{
								ObjetoPuertaAux[i]=ObjetoPuerta[i];
								EstadoVecAux[i]= EstadoVec[i];
								EsperarVecAux[i]= EsperarVec[i];
							}
						}
						ObjetoPuerta= new GameObject[ObjetoPuerta.Length+1];
						EstadoVec= new bool[ObjetoPuerta.Length];
						EsperarVec= new bool[ObjetoPuerta.Length];
						if(ObjetoPuerta.Length>=2)
						{
							for(int i=0;i<=ObjetoPuerta.Length-2;i++)
							{
								ObjetoPuerta[i]=ObjetoPuertaAux[i];
								EstadoVec[i]= EstadoVecAux[i];
								EsperarVec[i]= EsperarVecAux[i];
							}
						}
						ObjetoPuerta[ObjetoPuerta.Length-1]= ObjetoAnimado;
						Estado=false;
						Esperar=false;
						Puerta= ObjetoPuerta[ObjetoPuerta.Length-1].gameObject.transform.GetChild(1);// abierta/cerrada
					}
					//****************************************************************************
					if(Puerta.gameObject.activeSelf==false)
					{
						HijoAnimacion= ObjetoAnimado.gameObject.transform.GetChild(0);
						Animaciones=HijoAnimacion.GetComponent<Animator>();
						AnimacioPuerta();
					}
					else
					{
						if(Tiempo_audio+1<Time.time)
						{
							AudioSource.PlayClipAtPoint(Intentar_audio,transform.position);
							Tiempo_audio= Time.time;
						}
					}
				}
			}
		}
	}
	void AnimacioPuerta(){
		//realiza la animacion de apartura o cierre de la puerta
		if(Estado==true && Esperar==false)
		{
			AudioSource.PlayClipAtPoint(Cerrar_audio,transform.position);
			Animaciones.SetBool ("EstadoPuerta", false);
			Esperar=true;
			Estado=false;
			Tiempo= Time.time;
			if(Chekeado==false)
			{
				EstadoVec[ObjetoPuerta.Length-1]= Estado;
				EsperarVec[ObjetoPuerta.Length-1]=Esperar;
			}
			else
			{
				EstadoVec[posicion]= Estado;
				EsperarVec[posicion]=Esperar;
			}
		}
		if(Estado==false && Esperar==false)
		{
			AudioSource.PlayClipAtPoint(Abrir_audio,transform.position);
			Animaciones.SetBool ("EstadoPuerta", true);	
			Esperar=true;
			Estado=true;
			Tiempo= Time.time;
			if(Chekeado==false)
			{
				EstadoVec[ObjetoPuerta.Length-1]= Estado;
				EsperarVec[ObjetoPuerta.Length-1]=Esperar;
			}
			else
			{
				EstadoVec[posicion]= Estado;
				EsperarVec[posicion]=Esperar;
			}
		}
	}
}
