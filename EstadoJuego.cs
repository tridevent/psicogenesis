﻿using UnityEngine;
using System.Collections;

public class EstadoJuego : MonoBehaviour {
	public static bool Cinematicas=false;
	private bool Proceso=false;
	private float Tiempo=0;
	public GameObject Personaje; //para activar o desactivar al fpc
	public GameObject HUD;// para activar o desactivar al hud
	public GameObject Camara;// para activar o desactivar la cinematica inicial de arrastrar
	public Transform PosicionCamara; // detecta al hijo del objeto colisionado que es quien tiene la animacion
	public AudioSource EntrarCama_audio;
	public AudioSource SalirCama_audio;

	void Update () {
		CinematicaCama ();
	}
	void CinematicaCama(){
		// ejecuta la animacion de cuando la personaje se mete debajo de la cama
		if (Tiempo + 13 < Time.time && Proceso == Cinematicas) 
		{
			Cinematicas=false;
		}
		if(Proceso != Cinematicas)
		{
			if(Cinematicas== true)
			{
				Personaje.SetActive(false);
				HUD.SetActive(false);
				Camara.SetActive(true);
				Proceso=true;
				Tiempo=Time.time;
				EntrarCama_audio.Play(44100);
				SalirCama_audio.Play(396900);
				print ("si");
			}
			else
			{
				print("no");
				PosicionCamara= Personaje.gameObject.transform.GetChild(0);
				PosicionCamara.transform.rotation = Camara.transform.rotation;
				Personaje.gameObject.transform.GetChild(0).rotation= PosicionCamara.transform.rotation;
				Personaje.SetActive(true);
				HUD.SetActive(true);
				Camara.SetActive(false);
				Proceso=false;
				
			}
		}
	}
}
