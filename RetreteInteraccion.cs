﻿using UnityEngine;
using System.Collections;

public class RetreteInteraccion : MonoBehaviour {
	public Animator Animaciones; // detecta la animaciones de la puerta
	private bool Esperar=false; //estado si se puede o no interactuar con la puerta
	private bool Estado=false;//Estado si la puerta esta abierta o cerrada
	private float Tiempo;// variable temporal para la comprobacion de la espera
	public GameObject ObjetoAnimado; // detecta el objeto colisionado en este caso la puerta
	public Transform HijoAnimacion; // detecta al hijo del objeto colisionado que es quien tiene la animacion
	public AudioClip Abrir_audio;
	public AudioClip Cerrar_audio;
	public AudioClip Cadena_audio;
	private float Tiempo_audio;
	public GameObject[] ObjetoPuerta; //la tapa del retrete
	public bool[] EsperarVec; 
	public bool[] EstadoVec;
	private int posicion=0;
	
	void Update () {
		if(Esperar==true && Tiempo<Time.time)
		{
			Esperar=false;
			EstadoVec[posicion]= Estado;
			EsperarVec[posicion]=Esperar;
		}
	}
	void OnTriggerStay (Collider Col){
		if (Input.GetButtonUp ("interactuar")) 
		{
			if (Col.gameObject.tag == "retrete") 
			{
				ObjetoAnimado= Col.GetComponent<Collider>().gameObject;
				for(int i =0;i<=ObjetoPuerta.Length-1;i++)
				{
					if(ObjetoPuerta[i].GetInstanceID()== ObjetoAnimado.GetInstanceID())
					{
						Estado=EstadoVec[i];
						Esperar=EsperarVec[i];
						posicion=i;
					}
				}
				HijoAnimacion= ObjetoAnimado.gameObject.transform;
				Animaciones=HijoAnimacion.GetComponent<Animator>();
				AnimacioRetrete();
			}
		}
	}
	void AnimacioRetrete(){
		//administra la animacion de apertura o cierre del retrete
		if(Estado==true && Esperar==false)
		{
			AudioSource.PlayClipAtPoint(Cerrar_audio,transform.position);
			Animaciones.SetBool ("Estado", false);
			Esperar=true;
			Estado=false;
			Tiempo= Time.time;
			EstadoVec[posicion]= Estado;
			EsperarVec[posicion]=Esperar;
			AudioSource.PlayClipAtPoint(Cadena_audio,transform.position);
		}
		if(Estado==false && Esperar==false)
		{
			AudioSource.PlayClipAtPoint(Abrir_audio,transform.position);
			Animaciones.SetBool ("Estado", true);	
			Esperar=true;
			Estado=true;
			Tiempo= Time.time;
			EstadoVec[posicion]= Estado;
			EsperarVec[posicion]=Esperar;

		}
	}
}
