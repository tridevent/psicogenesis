using System;
using UnityEngine;

namespace UnityStandardAssets.ImageEffects
{
    [RequireComponent (typeof(Camera))]
    public class CerrarOjos : PostEffectsBase
    {
        public enum AberrationMode
        {
            Simple = 0,
            Advanced = 1,
        }

        public AberrationMode mode = AberrationMode.Simple;
        public float intensity = 0.375f;                    // intensity == 0 disables pre pass (optimization)
        public float chromaticAberration = 0.2f;
        public float axialAberration = 0.5f;
        public float blur = 0.0f;                           // blur == 0 disables blur pass (optimization)
        public float blurSpread = 0.75f;
        public float luminanceDependency = 0.25f;
        public float blurDistance = 2.5f;
        public Shader vignetteShader;
        public Shader separableBlurShader;
        public Shader chromAberrationShader;
        
        
        private Material m_VignetteMaterial;
        private Material m_SeparableBlurMaterial;
        private Material m_ChromAberrationMaterial;


        public override bool CheckResources ()
        {
            CheckSupport (false);

            m_VignetteMaterial = CheckShaderAndCreateMaterial (vignetteShader, m_VignetteMaterial);
            m_SeparableBlurMaterial = CheckShaderAndCreateMaterial (separableBlurShader, m_SeparableBlurMaterial);
            m_ChromAberrationMaterial = CheckShaderAndCreateMaterial (chromAberrationShader, m_ChromAberrationMaterial);

            if (!isSupported)
                ReportAutoDisable ();
            return isSupported;
        }


        void OnRenderImage (RenderTexture source, RenderTexture destination)
        {
            if ( CheckResources () == false)
            {
                Graphics.Blit (source, destination);
                return;
            }

            int rtW = source.width;
            int rtH = source.height;

            bool  doPrepass = (Mathf.Abs(blur)>0.0f || Mathf.Abs(intensity)>0.0f);

            float widthOverHeight = (1.0f * rtW) / (1.0f * rtH);
            const float oneOverBaseSize = 1.0f / 512.0f;

            RenderTexture color = null;
            RenderTexture color2A = null;

            if (doPrepass)
            {
                color = RenderTexture.GetTemporary (rtW, rtH, 0, source.format);

                // Blur corners
                if (Mathf.Abs (blur)>0.0f)
                {
                    color2A = RenderTexture.GetTemporary (rtW / 2, rtH / 2, 0, source.format);

                    Graphics.Blit (source, color2A, m_ChromAberrationMaterial, 0);

                    for(int i = 0; i < 2; i++)
                    {	// maybe make iteration count tweakable
                        m_SeparableBlurMaterial.SetVector ("offsets",new Vector4 (0.0f, blurSpread * oneOverBaseSize, 0.0f, 0.0f));
                        RenderTexture color2B = RenderTexture.GetTemporary (rtW / 2, rtH / 2, 0, source.format);
                        Graphics.Blit (color2A, color2B, m_SeparableBlurMaterial);
                        RenderTexture.ReleaseTemporary (color2A);

                        m_SeparableBlurMaterial.SetVector ("offsets",new Vector4 (blurSpread * oneOverBaseSize / widthOverHeight, 0.0f, 0.0f, 0.0f));
                        color2A = RenderTexture.GetTemporary (rtW / 2, rtH / 2, 0, source.format);
                        Graphics.Blit (color2B, color2A, m_SeparableBlurMaterial);
                        RenderTexture.ReleaseTemporary (color2B);
                    }
                }

                m_VignetteMaterial.SetFloat ("_Intensity", intensity);		// intensity for vignette
                m_VignetteMaterial.SetFloat ("_Blur", blur);					// blur intensity
                m_VignetteMaterial.SetTexture ("_VignetteTex", color2A);	// blurred texture

                Graphics.Blit (source, color, m_VignetteMaterial, 0);			// prepass blit: darken & blur corners
            }

            m_ChromAberrationMaterial.SetFloat ("_ChromaticAberration", chromaticAberration);
            m_ChromAberrationMaterial.SetFloat ("_AxialAberration", axialAberration);
            m_ChromAberrationMaterial.SetVector ("_BlurDistance", new Vector2 (-blurDistance, blurDistance));
            m_ChromAberrationMaterial.SetFloat ("_Luminance", 1.0f/Mathf.Max(Mathf.Epsilon, luminanceDependency));

            if (doPrepass) color.wrapMode = TextureWrapMode.Clamp;
            else source.wrapMode = TextureWrapMode.Clamp;
            Graphics.Blit (doPrepass ? color : source, destination, m_ChromAberrationMaterial, mode == AberrationMode.Advanced ? 2 : 1);

            RenderTexture.ReleaseTemporary (color);
            RenderTexture.ReleaseTemporary (color2A);
        }
//**********************************************************************************************
		//DECLARACION DE VARIABLES
//***********************************************************************************************
		//************************
		public GameObject PantallaNegra; // plano negro en pantalla
		//private bool Cerrado=false; // estado ojos cerrados
		//private bool Abierto=false;// estado ojos abiertos
		//public bool Activar=false; // activa el efecto
		private float Tiempo=0; // variable temporal para utilizar en duracion
		public float Duracion=0; // duracion de la tansicion
		//public GameObject Personaje; //para activar o desactivar al fpc
		//public GameObject HUD;// para activar o desactivar al hud
		//public GameObject Camara;// para activar o desactivar la cinematica inicial de arrastrar
		public static bool InGame;
		public static bool Terminado;
		//************************
		void Update(){
			if (InGame == false) 
			{
				if(Tiempo+Duracion<Time.time)
				{
				CerrarOjosFn();
					Debug.Log ("cierra ojos");
				}

			}
			else
			{
				if( Terminado == false)
				{
				AbrirOjosFn();
					Debug.Log ("abre ojos");
				}
			}
			/*if( && Activar==false)
			{
				Activar=true;
				Tiempo=Time.time;
			}*/
			/*if(Activar==true)
			{

			}*/

		}
		void CerrarOjosFn(){
			//if (Cerrado == false) 
			//{
				if (intensity < 40) 
				{
					intensity +=20f*Time.deltaTime;
					blur += 20f*Time.deltaTime;
					blurSpread+= 20f*Time.deltaTime ;
				} else
				{
					PantallaNegra.SetActive(true);
					//Cerrado=true;
					//Tiempo= Time.time;
					InGame= true;
					Application.LoadLevel (1);
				}
			//}
			/*else
			{
				//if(Tiempo+1<Time.time)
				if(InGame==true) 
				{

				}
			}*/
		}
		void AbrirOjosFn(){
			//Personaje.SetActive(true);
			//HUD.SetActive(true);
			//Camara.SetActive(false);
			//if(Abierto==false)
			//{
				PantallaNegra.SetActive(false);
				blur=40;
				//Abierto=true;
				
			//}
			if (intensity > 1) 
			{
				intensity -= 10f*Time.deltaTime;
				blur -= 10f*Time.deltaTime;
				blurSpread-= 10f*Time.deltaTime ;
			}
			else
			{
				//Activar=false;
				intensity = 0;
				blur = 0;
				blurSpread= 0;
				Terminado = true;
				enabled= false;
			}
		}
	}
}
