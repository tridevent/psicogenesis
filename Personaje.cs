﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;


public class Personaje : MonoBehaviour {

	//********************************Publicas
	public GameObject[] PrefabsObjetos;// Libreria de objetos interactivos ***** 0 tarjeta , 1 llave , 2 botella, 3 vendas, 4 molotov , 5 encendedor
	public int [] ObjetosManos;// contiene el id de los objetos que cargas en las manos
	public bool [] HudObjetosManos;//contiene el estado de las manos vacias
	public Sprite[] HudManos;// Libreria los HUD pertenecientes a los objetos interactivos ***
	public Image Mano1;// objeto hud mano 1
	public Image Mano2;// objeto hud mano 2
	public GameObject[] ObjetosPuestos;// objetos interactivos que cargas en las manos
	public GameObject[] ObjetoSeleccionado;// objeto que actualmente tienes seleccionado en tu hud
	public Camera camara;// se utiliza para tomar las posiciones de donde esta mirando el personaje , se usa para tirar los objetos.
	public GameObject PosicionObjeto;// posicion en donde se tira el objeto interactivo.
	public Text Consola;// objeto texto que sirve para imprimir mensajes en la pantalla.
	public static bool TengoTarjeta=false;// variable que chequea el estado si se tiene o no la tarjeta de acceso
	public AudioClip TomarObjeto_audio; //sonido tomar un objeto
	public AudioClip TirarObjeto_audio;// sonido tirar un objeto
	public AudioSource Goteo_audio; // control sonido de goteo de fondo
	public Transform PosicionLibro; // posicion en donde aparece el libro que cayo
	public Transform Libro; // prefabs del libro instanciado
	public Transform PlatoRoto; // prefabs del plato instanciado
	public Transform PosicionPlato; // posicion en donde aparece el plato que cayo
	public AudioClip[] Maestro_audio; // maestro de sonido, 0 libro, 1 scream, 2 telefono, 3 baño
	public bool[] CheckMaestro; // check maestro
	//******************************************Privadas
	private bool LanzamientoMolotov= false;// variable que chequea el estado si se entra o no a la cinematica del lanzamiento de molotov
	private GameObject ObjetoDestruir;// variable temporal que almacena el objeto recolectado
	private bool ObjetoVisto=false;// variable que chequea si se vio o no al objeto interactivo
	private bool NoDestruir=false;// variable de estado que evalua cuando un objeto no debe ser destruido a causa de no poder ser recolectado.
	private int DistanciaRayo=10;// distancia de la vision del personaje
	private bool ValorSeleccionado;//chequea que objeto del hud tienes seleccionado 
	private CreaseShading Resplandor;//script externo
	private float Tiempo;//variable temporar para PararTiempo
	private bool PararTiempo = false;// variable que contiene el estado si se muestra o no un cartel en pantalla.
	RaycastHit hit;
	//******************************************
	void Start () {
		Resplandor = GameObject.Find ("Camara objetos").GetComponent<CreaseShading> ();
		Resplandor.enabled = false;
	}
	//******************************************
	void Update () {
		TiempoMensaje ();
		SeleccionFn();
		VisionObjetos();
		TirarObjetoFn ();
	}
	//******************************************
	void TiempoMensaje(){
		// administra el tiempo que esta visible los mensajes mostrados en pantalla
		if (PararTiempo==true && Tiempo + 2 < Time.time) 
		{
			Consola.text="";
			PararTiempo=false;
		}
	}
	//******************************************
	void SeleccionFn(){
		// administra el objeto del inventario activo.
		if (Input.GetButtonUp ("seleccion")) 
		{
			ValorSeleccionado=! ValorSeleccionado;
			if(ValorSeleccionado==false)
			{
				ObjetoSeleccionado[0].SetActive(true);
				ObjetoSeleccionado[1].SetActive(false);
			}
			else
			{
				ObjetoSeleccionado[1].SetActive(true);
				ObjetoSeleccionado[0].SetActive(false);
			}
		}
	}
	//******************************************
	void VisionObjetos(){
		// administra la vision de los objetos vistos , ya que si estan visto se debe activar un resplandor
		Debug.DrawRay(camara.transform.position, camara.transform.forward*DistanciaRayo, Color.green);
		//Debug.DrawRay(camara.transform.position, camara.transform.forward*DistanciaRayo, Color.red);
		if (Physics.Raycast (camara.transform.position, camara.transform.forward,out hit, DistanciaRayo)) 
		{
			if(hit.collider.gameObject.tag == "tarjeta" || hit.collider.gameObject.tag == "llave" || hit.collider.gameObject.tag == "botella" || hit.collider.gameObject.tag == "venda" || hit.collider.gameObject.tag == "molotov" || hit.collider.gameObject.tag == "mechero")
			{
				Resplandor.enabled=true;
				ObjetoVisto=true;
			}
		}
	}
	//******************************************
	void TirarObjetoFn(){
		// administra el inventario cuando se tira una objeto.
		if (Input.GetButtonUp ("tirar")) {
			if (Physics.Raycast (camara.transform.position, camara.transform.forward,out hit, DistanciaRayo)) 
			{
				if(hit.collider.gameObject.tag != "Untagged")
				{
					PosicionDejar();
				}
				else
				{
					if(ValorSeleccionado==false)
					{
						if(ObjetosPuestos[0]!=null)
						{
							TirarObjeto(0,ObjetosManos[0]);
						}
						
					}
					else
					{
						if(ObjetosPuestos[1]!=null)
						{
							TirarObjeto(1,ObjetosManos[1]);
						}
					}
				}
			}
		}
	}
	//******************************************
	void TomarObjeto (int id){
		// administra el objeto tomado 
		NoDestruir = false;
		if (ObjetosManos [0] == 0) 
		{
			ObjetosManos [0] = id;
			AudioSource.PlayClipAtPoint(TomarObjeto_audio,transform.position);
			HUD (id);
		} 
		else 
		{
			if (ObjetosManos [1] == 0) 
			{
				ObjetosManos [1] = id;
				AudioSource.PlayClipAtPoint(TomarObjeto_audio,transform.position);
				HUD (id);
			}
			else
			{
				NoDestruir=true;
				Consola.text = "Do not take more objects";
				Tiempo = Time.time;
				PararTiempo = true;
			}
		}
		if((ObjetosManos[0]==3 && ObjetosManos[1]==4) || (ObjetosManos[0]==4 && ObjetosManos[1]==3))
		{
			//aca va algo
			ObjetosManos[0]=5;
			ObjetosManos[1]=0;
			ObjetosPuestos[0]=PrefabsObjetos[4];
			ObjetosPuestos[1]=null;
			Mano1.sprite=HudManos[5];
			Mano2.sprite=HudManos[0];
			HudObjetosManos[0]=true;
			HudObjetosManos[1]=false;
		}
		if((ObjetosManos[0]==5 && ObjetosManos[1]==6) || (ObjetosManos[0]==6 && ObjetosManos[1]==5))
		{
			//aca va algo
			LanzamientoMolotov= true;
		}

	}
	//******************************************
	void HUD(int id){
		// administra el inventario de la personaje
		if (HudObjetosManos [0] == false) 
		{
			Mano1.sprite=HudManos[id];
			HudObjetosManos[0]= true;
			ObjetosPuestos[0]= PrefabsObjetos[id-1];
		} 
		else 
		{
			if (HudObjetosManos [1] == false) 
			{
				Mano2.sprite=HudManos[id];
				HudObjetosManos[1]= true;
				ObjetosPuestos[1]= PrefabsObjetos[id-1];
			}
		}
	}
	//******************************************
	void TirarObjeto(int id, int idObjeto){
		//  administra el donde se debe colocar el objeto tirado
		Instantiate (ObjetosPuestos [id],PosicionObjeto.transform.position,transform.rotation);
		if (idObjeto == 1) 
		{
			TengoTarjeta=false;
		}
		if (id == 0) 
		{
			ObjetosManos [0] = 0;
			HudObjetosManos [0] = false;
			Mano1.sprite=HudManos[0];
			AudioSource.PlayClipAtPoint(TirarObjeto_audio,transform.position);
		}
		else 
		{
			ObjetosManos [1] = 0;
			HudObjetosManos [1] = false;
			Mano2.sprite=HudManos[0];
			AudioSource.PlayClipAtPoint(TirarObjeto_audio,transform.position);
		}
		ObjetosPuestos [id] = null;
	}
	//******************************************
	void OnTriggerEnter(Collider Col){
		// administra los desatadores de eventos. 
		if (LanzamientoMolotov == true) 
		{
			//cinematica lanzamiento molotov
		}
		if (Col.gameObject.tag == "sonidocelda") 
		{
			Goteo_audio.Play(44100);
		}
		if (Col.gameObject.tag == "librocaer") 
		{
			if(CheckMaestro[0]==false)
			{
				Instantiate(Libro,PosicionLibro.position,Quaternion.identity);
				AudioSource.PlayClipAtPoint(Maestro_audio[0],transform.position);
				CheckMaestro[0]=true;
			}
		}
		if (Col.gameObject.tag == "scream") 
		{
			if(CheckMaestro[1]==false)
			{
				AudioSource.PlayClipAtPoint(Maestro_audio[1],transform.position);
				CheckMaestro[1]=true;
			}
		}
		if (Col.gameObject.tag == "telefono") 
		{
			if(CheckMaestro[2]==false)
			{
				AudioSource.PlayClipAtPoint(Maestro_audio[2],transform.position);
				CheckMaestro[2]=true;
			}
		}
		if (Col.gameObject.tag == "baño") 
		{
			if(CheckMaestro[3]==false)
			{
				AudioSource.PlayClipAtPoint(Maestro_audio[3],transform.position);
				CheckMaestro[3]=true;
			}
		}
		if (Col.gameObject.tag == "teatro_1") 
		{
			if(CheckMaestro[4]==false)
			{
				AudioSource.PlayClipAtPoint(Maestro_audio[4],transform.position);
				CheckMaestro[4]=true;
			}
		}
		if (Col.gameObject.tag == "teatro_2") 
		{
			if(CheckMaestro[5]==false)
			{
				AudioSource.PlayClipAtPoint(Maestro_audio[5],transform.position);
				CheckMaestro[5]=true;
			}
		}
		if (Col.gameObject.tag == "cocina") 
		{
			if(CheckMaestro[6]==false)
			{
				AudioSource.PlayClipAtPoint(Maestro_audio[6],transform.position);
				CheckMaestro[6]=true;
			}
		}
		if (Col.gameObject.tag == "comedor") 
		{
			if(CheckMaestro[7]==false)
			{
				Instantiate(PlatoRoto,PosicionPlato.position,Quaternion.identity);
				AudioSource.PlayClipAtPoint(Maestro_audio[7],transform.position);
				CheckMaestro[7]=true;
			}
		}
	}
	void OnTriggerExit(Collider Col){
		if (Col.gameObject.tag == "sonidocelda") 
		{
			Goteo_audio.Stop();
		}
	}
	void OnTriggerStay (Collider Col){
		//administra si estan dentro de un evento para  interactuar.
		if (ObjetoVisto == true && Input.GetButtonUp ("interactuar")) 
		{
			if (Col.gameObject.tag == "tarjeta") 
			{
				TomarObjeto (1);
				TengoTarjeta=true;
			}
			if (Col.gameObject.tag == "llave") 
			{
				TomarObjeto (2);
			}
			if (Col.gameObject.tag == "botella") 
			{
				TomarObjeto (3);

			}
			if (Col.gameObject.tag == "venda") 
			{
				TomarObjeto (4);
				
			}
			if (Col.gameObject.tag == "molotov") 
			{
				TomarObjeto (5);
				
			}
			if (Col.gameObject.tag == "mechero") 
			{
				TomarObjeto (6);
				
			}
			if (Col.gameObject.tag == "tarjeta" || Col.gameObject.tag == "llave" || Col.gameObject.tag == "botella" || Col.gameObject.tag == "venda" || Col.gameObject.tag == "molotov" || Col.gameObject.tag == "mechero") 
			{
				if (NoDestruir == false) {
					ObjetoDestruir = Col.GetComponent<Collider>().gameObject;
					ObjetoVisto=false;
					Resplandor.enabled = false;
					Destroy (ObjetoDestruir);
				}
			}
		}
	}
	//******************************************
	void PosicionDejar(){
		// Administra el mensaje que saldra en pantalla cuando se necesite mostrar.
		Consola.text = "you can not place object";
		Tiempo = Time.time;
		PararTiempo = true;
	}
}
