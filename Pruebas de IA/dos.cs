﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class dos : MonoBehaviour {
	private RaycastHit hit;
	private LayerMask mascara;
	private Transform Player;
	private List<GameObject> wayPointsList;
	void Start(){
		Player = GameObject.FindGameObjectWithTag ("Player").transform;
		wayPointsList = new List<GameObject> ();
		GameObject[] wayPoints = GameObject.FindGameObjectsWithTag ("wayPoint");
		Player.GetComponent<Renderer> ().material.color = Color.blue;
		transform.GetComponent<Renderer>().material.color = Color.red;
		foreach (GameObject newWayPoint in wayPoints)
		{
			wayPointsList.Add(newWayPoint);
		}
	}
	void Update(){
		mascara.value = 9;
		//print (mascara.value);
		bool wayPointToTargetCollision2 = Physics.Linecast(transform.position,Player.position,out hit,mascara.value);
		//if (wayPointToTargetCollision2==true) {
			Follow ();
		//}
		RaycastHit[] hits;
		hits = Physics.RaycastAll(transform.position, transform.forward, 100.0F);
		int i = 0;
		for(i=0;i < hits.Length;i++) {
			RaycastHit hit2 = hits [i];
			if(hit2.collider.gameObject.tag=="Player")
			{
				print ("te encontre");
			}
		}

	}
	void Follow(){
		GameObject wayPoint = null;
		if(Physics.Linecast(transform.position, Player.position))
		{
			wayPoint = FindBetterWay();
		}
		else
		{
			wayPoint = GameObject.FindGameObjectWithTag("Player");
		}
		Vector3 Dir = (wayPoint.transform.position - transform.position).normalized;
		transform.position += Dir * Time.deltaTime * 5;
		transform.rotation = Quaternion.LookRotation (Dir);
	}
	GameObject FindBetterWay(){
		GameObject betterWay = null;
		float distanceToBetterWay = Mathf.Infinity;
		foreach (GameObject go in wayPointsList) 
		{
			float distToWayPoint= Vector3.Distance(transform.position,go.transform.position);
			float distWayPointToTarget= Vector3.Distance (go.transform.position,Player.position);
			float distToTarget = Vector3.Distance(transform.position,Player.position);
			bool wallBetween = Physics.Linecast(transform.position,go.transform.position);
			if((distToWayPoint < distanceToBetterWay)&& (distToTarget > distWayPointToTarget) && (!wallBetween))
			{
				distanceToBetterWay= distToWayPoint;
				betterWay= go;
			}
			else
			{
				bool wayPointToTargetCollision = Physics.Linecast(go.transform.position,Player.position);
				if(!wayPointToTargetCollision)
				{
					betterWay = go;
				}
			}
		}
		return betterWay;
	}
}
