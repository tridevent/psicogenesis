﻿using UnityEngine;
using System.Collections;

public class InteligenciaArtificial: MonoBehaviour {

	public Transform target;
	internal Animator animator;
	internal NavMeshAgent agent;
	public Transform[] Waypoint;
	public Transform[] Waypoint_1;
	public Transform[] Waypoint_2;
	public Transform[] Waypoint_3;
	public Transform[] Waypoint_4;
	public Transform[] Waypoint_5;
	public int aleatorio=0;
	public Transform Jugador;
	public float DistanceToWP = 1;
	public float Speed = 5;
	public bool Loop= true;
	public bool Encontrado=false;
	public int DistanciaRayo=10;

	void Awake(){
		animator = GetComponent<Animator>();
		agent = GetComponent<NavMeshAgent>();
		CambioRuta ();
		//SI NO HAY WP DESACTIVAR EL SCRIPT.
		if(Waypoint.Length <= 0)
		{
			enabled=false;
			Debug.LogWarning("No WayPoint Loaded in: "+name);
		}
	}
	void CambioRuta(){
		aleatorio = Random.Range (1, 6);
		if(aleatorio==1)
		{
			for(int i=0; i<Waypoint.Length; i++)
			{
				Waypoint[i]= Waypoint_1[i];
			}
		}
		if(aleatorio==2)
		{
			for(int i=0; i<Waypoint.Length; i++)
			{
				Waypoint[i]= Waypoint_2[i];
			}
		}
		if(aleatorio==3)
		{
			for(int i=0; i<Waypoint.Length; i++)
			{
				Waypoint[i]= Waypoint_3[i];
			}
		}
		if(aleatorio==4)
		{
			for(int i=0; i<Waypoint.Length; i++)
			{
				Waypoint[i]= Waypoint_4[i];
			}
		}
		if(aleatorio==5)
		{
			for(int i=0; i<Waypoint.Length; i++)
			{
				Waypoint[i]= Waypoint_5[i];
			}
		}
	}
	//CONECCION A LOS WAYPOINTS
	void OnDrawGizmos(){
		if (Waypoint.Length > 0) 
		{
			for( int i=0; i< Waypoint.Length; i++) 
			{
				//Si hay mas de 1 WP se inician las conexiones.
				if (i>0)
				{
					//inicia del ultimo WP hacia atras, para que no sobre ningun WP (resta).
					Gizmos.color = Color.blue;
					Gizmos.DrawLine(Waypoint[i].position, Waypoint[i-1].position);
				}
			
			}
		}
	}
	//Variable no publica para iniciar indicar a que Waypoint caminara al llegar a un WP.
	private int NextWP = 0;
	//CAMINAR HACIA LOSWAYPOINTS

	void Update(){
		Debug.DrawLine(transform.position, target.transform.position, Color.green);
		RaycastHit hit;
		Vector3 DireccionObjetivo = target.transform.position - transform.position;
		float fDetectado = Vector3.Angle (DireccionObjetivo, transform.forward);
		print ("distancia: "+fDetectado);
		if (Physics.Raycast (transform.position,target.transform.position,out hit,100)) 
		{
			if(fDetectado <= 45f)
			{
				print ("angulo de vision");
				if(hit.collider.gameObject.tag == "Player")
				{
					print ("encontrado");
					Encontrado=true;
				}
			}

		}
	}
	void FixedUpdate(){

		if( Encontrado==true)
		{
			agent.destination = target.position;
			/*// Waypoint - Posicion del personaje.
			float vChaToPJ = Vector3.Distance(Jugador.position, transform.position);
			//Caminar hacia el WP en referencia de la distancia
			if (vChaToPJ >= DistanceToWP) 
			{
				transform.LookAt (new Vector3 (Jugador.position.x, transform.position.y, Jugador.position.z) );
				transform.position += transform.forward * Time.deltaTime * Speed;
			}*/
		}
		else
		{
			// Waypoint - Posicion del personaje.
			float vChaToWP = Vector3.Distance(Waypoint[NextWP].position, transform.position);
			//Caminar hacia el WP en referencia de la distancia
			if (vChaToWP >= DistanceToWP) 
			{
				//transform.LookAt (new Vector3 (Waypoint[NextWP].position.x, transform.position.y, Waypoint[NextWP].position.z) );
				//transform.position += transform.forward * Time.deltaTime * Speed;
				agent.destination = Waypoint[NextWP].position;
			}
			else
			{
				// Obtenemos el total de WP y le restamos 1, para que no llegue al final y trate de ir a otro.
				if (NextWP < Waypoint.Length -1)
				{
					//Hacer que se pase al siguiente WP a caminar, sumando 1 al actual.
					NextWP++;
				}
				else 
				{
					//Si no hay Loop activado desactivar Script (rendimiento).
					if (Loop)
					{
						// Si se llega al ultimo WP, hacemos que se repita
						NextWP = 0;
						CambioRuta();
					}else
					{
						//Si no hay Loop activado desactivar Script (rendimiento).
						enabled =false;
					}
				}
			}
		}
	}
}