﻿using UnityEngine;
using System.Collections;

public class PuertaDigital : MonoBehaviour {
	public static bool Permitido= false; // estado si se permite o no abrir la puerta
	public AudioClip Aceptado_audio;
	public AudioClip Rechazado_audio;

	void OnTriggerStay (Collider Col){
		if (Col.gameObject.tag == "Tarjetero")
		{
			if(Personaje.TengoTarjeta==true && Input.GetButtonUp("interactuar"))
			{
				Permitido= true;
				AudioSource.PlayClipAtPoint(Aceptado_audio,transform.position);
			}
			if(Personaje.TengoTarjeta==false && Input.GetButtonUp("interactuar"))
			{
				AudioSource.PlayClipAtPoint(Rechazado_audio,transform.position);
			}
		}
	}
}
