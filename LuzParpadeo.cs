﻿using UnityEngine;
using System.Collections;

public class LuzParpadeo : MonoBehaviour {
	public Light luz; // almacena las configuraciones de la luz
	public GameObject focos; // los obj del foco
	private int Tiempo=0; 
	private int Tiempo2=0;
	private bool estado; // el estado del foco
	void Start () {
		InvokeRepeating ("Parpadear", 1, 1);
	}
	void Parpadear () {
		// hace que la luz parpadee
		if(Tiempo<2 && estado==false)
		{
			Tiempo++;
		}
		else
		{
			Tiempo=0;
			estado=true;
			Renderer rend = focos.GetComponent<Renderer>();
			rend.materials[1].SetColor("_EmissionColor", Color.white);
		}
	}
	void Update(){
		// hace que la luz prenda o se apague
		if( estado== true)
		{

			luz.enabled=!luz.enabled;
			Tiempo2++;
			if(Tiempo2>250)
			{
				estado=false;
				Tiempo2=0;
			}
		}
		else
		{
			if(luz.enabled==false)
			{
				Renderer rend = focos.GetComponent<Renderer>();
				rend.materials[1].SetColor("_EmissionColor", Color.black);
			}
		}
	}
}
